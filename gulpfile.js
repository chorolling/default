const conf = require('./config.js');
const webpackConfig = require("./webpack.config");

const gulp = require('gulp');
const watch = require('gulp-watch');
const pug = require('gulp-pug');
const data = require('gulp-data');
const webpackStream = require("webpack-stream");
const webpack = require("webpack");
const notify = require('gulp-notify');
const plumber = require('gulp-plumber');
const connect = require('gulp-connect');
const stylus = require('gulp-stylus');
const autoprefixer = require('autoprefixer-stylus');
const changed  = require('gulp-changed');
const imagemin = require('gulp-imagemin');


gulp.task('connect', function() {
  connect.server({
    root: conf.path.dest,
    livereload: false
  });
});


// ----------------------------------------- STYLUS
gulp.task('stylus:dev', () => {
    gulp.src([
        conf.path.src+'style/[^_]*.styl',
        conf.path.src+'style/[^_]**/[^_]*.styl',
        conf.path.src+'style/[^_]**/[^_]**/[^_]*.styl',
        conf.path.src+'style/[^_]**/[^_]**/[^_]**/[^_]*.styl'
    ])
    .pipe(plumber({
        errorHandler: notify.onError("Error: <%= error.message %>")
    }))
    .pipe(stylus({
        use: [ autoprefixer({ browsers: [ 'iOS >= 8', 'last 2 versions' ]}) ]
    }))
    .pipe(gulp.dest(conf.path.dest))
    .pipe(notify('Complete: <%= file.relative %>'))
});
gulp.task('stylus:deploy', () => {
    gulp.src([
        conf.path.src+'style/[^_]*.styl',
        conf.path.src+'style/[^_]**/[^_]*.styl',
        conf.path.src+'style/[^_]**/[^_]**/[^_]*.styl',
        conf.path.src+'style/[^_]**/[^_]**/[^_]**/[^_]*.styl'
    ])
    .pipe(plumber({
        errorHandler: notify.onError("Error: <%= error.message %>")
    }))
    .pipe(stylus({
        use: [ autoprefixer({ browsers: [ 'iOS >= 8', 'last 2 versions' ]}) ],
        compress: true
    }))
    .pipe(gulp.dest(conf.path.dest))
    .pipe(notify('Complete: <%= file.relative %>'))
});


// ----------------------------------------- PUG
gulp.task('pug:dev', () => {
    gulp.src([
        conf.path.src+'[^_]*.pug',
        conf.path.src+'[^_]**/[^_]*.pug',
        conf.path.src+'[^_]**/[^_]**/[^_]*.pug',
        conf.path.src+'[^_]**/[^_]**/[^_]**/[^_]*.pug'
    ])
    .pipe(plumber({
        errorHandler: notify.onError("Error: <%= error.message %>")
    }))
    .pipe( data( function (file) {
        return { 'data': require(conf.path.src+'data.json')};
    }))
    .pipe(pug({
        pretty: true
    }))
    .pipe(gulp.dest(conf.path.dest))
    .pipe(notify('Complete: <%= file.relative %>'))
});

gulp.task('pug:deploy', () => {
    gulp.src([
        conf.path.src+'[^_]*.pug',
        conf.path.src+'[^_]**/[^_]*.pug',
        conf.path.src+'[^_]**/[^_]**/[^_]*.pug',
        conf.path.src+'[^_]**/[^_]**/[^_]**/[^_]*.pug'
    ])
    .pipe(plumber({
        errorHandler: notify.onError("Error: <%= error.message %>")
    }))
    .pipe( data( function (file) {
        return { 'data': require(conf.path.src+'data.json')};
    }))
    .pipe(pug({
        pretty: false
    }))
    .pipe(gulp.dest(conf.path.dest))
    .pipe(notify('Complete: <%= file.relative %>'))
});

// ----------------------------------------- WEBPACK

gulp.task('webpack:dev', function() {
    gulp.src([
        conf.path.src+'js/[^_]*.js',
        conf.path.src+'js/[^_]**/[^_]*.js',
        conf.path.src+'js/[^_]**/[^_]**/[^_]*.js',
        conf.path.src+'js/[^_]**/[^_]**/[^_]**/[^_]*.js'
    ])
    .pipe(plumber({
        errorHandler: notify.onError("Error: <%= error.message %>")
    }))
    .pipe(webpackStream(webpackConfig.dev, webpack))
    .pipe(gulp.dest(conf.path.dest))
    .pipe(notify('Complete: <%= file.relative %>'))
});


gulp.task('webpack:deploy', function() {
    gulp.src([
        conf.path.src+'js/[^_]*.js',
        conf.path.src+'js/[^_]**/[^_]*.js',
        conf.path.src+'js/[^_]**/[^_]**/[^_]*.js',
        conf.path.src+'js/[^_]**/[^_]**/[^_]**/[^_]*.js'
    ])
    .pipe(plumber({
        errorHandler: notify.onError("Error: <%= error.message %>")
    }))
    .pipe(webpackStream(webpackConfig.deploy, webpack))
    .pipe(gulp.dest(conf.path.dest))
    .pipe(notify('Complete: <%= file.relative %>'))
});

// ----------------------------------------- IMAGE
gulp.task('imagemin:dev', function() {
    gulp.src([
        conf.path.src+'[^_]*.+(jpg|jpeg|png|gif|svg)',
        conf.path.src+'[^_]**/[^_]*.+(jpg|jpeg|png|gif|svg)',
        conf.path.src+'[^_]**/[^_]**/[^_]*.+(jpg|jpeg|png|gif|svg)',
        conf.path.src+'[^_]**/[^_]**/[^_]**/[^_]*.+(jpg|jpeg|png|gif|svg)'
    ])
    .pipe(plumber({
        errorHandler: notify.onError("Error: <%= error.message %>")
    }))
    .pipe(changed(conf.path.dest))
    .pipe(imagemin([
        imagemin.jpegtran({progressive: true}),
        imagemin.gifsicle({
            interlaced: false,
            optimizationLevel: 3,
            colors:256
        }),
        imagemin.svgo({
            plugins: [
                {removeViewBox: true},
                {cleanupIDs: true}
            ]
        })
    ]))
    .pipe(gulp.dest(conf.path.dest))
    .pipe(notify('Complete: <%= file.relative %>'))
});


gulp.task('imagemin:deploy', function() {
    gulp.src([
        conf.path.src+'[^_]*.+(jpg|jpeg|png|gif|svg)',
        conf.path.src+'[^_]**/[^_]*.+(jpg|jpeg|png|gif|svg)',
        conf.path.src+'[^_]**/[^_]**/[^_]*.+(jpg|jpeg|png|gif|svg)',
        conf.path.src+'[^_]**/[^_]**/[^_]**/[^_]*.+(jpg|jpeg|png|gif|svg)'
    ])
    .pipe(plumber({
        errorHandler: notify.onError("Error: <%= error.message %>")
    }))
    .pipe(imagemin([
        imagemin.jpegtran({progressive: true}),
        imagemin.optipng({optimizationLevel: 7}),
        imagemin.gifsicle({
            interlaced: false,
            optimizationLevel: 3,
            colors:256
        }),
        imagemin.svgo({
            plugins: [
                {removeViewBox: true},
                {cleanupIDs: true},
                {removeEmptyAttrs: true},
                {removeEmptyContainers: true},
                {minifyStyles: true}
            ]
        })
    ]))
    .pipe(gulp.dest(conf.path.dest))
    .pipe(notify('Complete: <%= file.relative %>'))
});


// ----------------------------------------- WATCH
gulp.task('watch', () => {
    watch(conf.path.src+'style/**/*.styl', () => {
        gulp.start(['stylus:dev']);
    });

    watch([conf.path.src+'**/*.json', conf.path.src+'**/*.pug'], () => {
        gulp.start(['pug:dev']);
    });

    watch(conf.path.src+'js/**/*.js', () => {
        gulp.start(['webpack:dev']);
    });

    watch(conf.path.src+'images/**/*.+(jpg|jpeg|png|gif|svg)', () => {
        gulp.start(['imagemin:dev']);
    });

})

gulp.task('dev', () => {
    gulp.start(['stylus:dev']);
    gulp.start(['pug:dev']);
    gulp.start(['webpack:dev']);
    gulp.start(['imagemin:dev']);
})
gulp.task('deploy', () => {
    gulp.start(['stylus:deploy']);
    gulp.start(['pug:deploy']);
    gulp.start(['webpack:deploy']);
    gulp.start(['imagemin:deploy']);
})

gulp.task('default', ['connect', 'build:dev', 'watch']);
gulp.task('s', ['connect', 'watch']);
gulp.task('build:dev', ['dev']);
gulp.task('build:deploy', ['deploy']);
