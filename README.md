# init
`npm install`

# command
* `gulp`: 圧縮なしbuild, 変更あり画像圧縮, server, watch
* `gulp s`: server, watch
* `gulp build:dev`: 圧縮なしbuild, 変更あり画像圧縮
* `gulp build:deploy`: 圧縮ありbuild, 全画像圧縮
