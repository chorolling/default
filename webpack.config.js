const webpack = require("webpack");

let config = {
    cache: true,
    entry: {
        app: './_src/js/index.js'
    },
    output: {
        'filename': '[name].js'
    },
    plugins: [
        new webpack.optimize.UglifyJsPlugin()
    ],
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "babel-loader",
            options:{
                presets: [
                    ["env", { "targets": {"browsers": ["last 2 versions"]} }]]
            }
        }]
    }
}

let dev = Object.assign({}, config);
dev['devtool'] = "#inline-source-map";
delete dev.plugins;


module.exports = {
    dev: dev, deploy: config
}
