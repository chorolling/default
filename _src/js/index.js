require("expose-loader?$!../../node_modules/jquery/dist/jquery.min.js")

let Maps = require("./class/maps.js")

;((win, doc, $) => {

    $(() => {
        let maps = new Maps()
        win.initMap = maps.init()

    })

})(window, document, $)
