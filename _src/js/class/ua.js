export default class UA {
	constructor() {
		let ua = {};
		ua.name = navigator.userAgent.toLowerCase();
		ua.isIphone = /iphone/.test(ua.name);
		ua.isIpod = /ipod/.test(ua.name);
		ua.isIpad = /ipad/.test(ua.name);
		ua.isIos = ua.isIphone || ua.isIpod || ua.isIpad;
		ua.isAndroid = /android/.test(ua.name);
		ua.isSp = (ua.isIos || ua.isAndroid) && win.innerWidth < 768;
		ua.isPc = !ua.isSp;
		ua.isMac = false;

		if (ua.isPc) {
			ua.isMac = /mac os/.test(ua.name);

			ua.isIe = /msie/.test(ua.name) || /trident/.test(ua.name) || /edge/.test(ua.name);
			ua.isOpera = /opera/.test(ua.name) || /opr/.test(ua.name);
			ua.isChrome = !ua.isIe && !ua.isOpera && /chrome/.test(ua.name);
			ua.isSafari = !ua.isIe && !ua.isOpera && !ua.isChrome && /safari/.test(ua.name);
			ua.isFirefox = /firefox/.test(ua.name);

			if (ua.isIe) {
				ua.isIe = true;
				ua.appVer = navigator.appVersion.toLowerCase();
				if (/edge/.test(ua.name)) {
					ua.ver = 12;
				} else if (ua.appVer.indexOf("rv:11.") != -1) {
					ua.ver = 11;
				} else if (ua.appVer.indexOf("msie 10.") != -1) {
					ua.ver = 10;
				} else if (ua.appVer.indexOf("msie 9.") != -1) {
					ua.ver = 9;
				} else if (ua.appVer.indexOf("msie 8.") != -1) {
					ua.ver = 8;
				} else if (ua.appVer.indexOf("msie 7.") != -1) {
					ua.ver = 7;
				} else if (ua.appVer.indexOf("msie 6.") != -1) {
					ua.ver = 6;
				}
			}
		} else {
			if (ua.isIos) {
				ua.isChrome = /crios/.test(ua.name);
				if (ua.isChrome) {
					ua.ver_array = /(iphone; cpu iphone os )([0-9]+_[0-9]+)/i.exec(ua.name);
				} else {
					ua.ver_array = /(version)\/([0-9]+.[0-9]+)/i.exec(ua.name);
				}
				if (ua.ver_array) {
					ua.ver = Number(ua.ver_array[2].replace('_', '.'));
				}
			}
			if (ua.isAndroid) {
				ua.isChrome = /chrome/.test(ua.name);
				ua.ver_array = /(android)\s([0-9]+.[0-9]+)/.exec(ua.name);
				if (ua.ver_array) {
					ua.ver = Number(ua.ver_array[2]);
				}
			}
		}
	}

}
