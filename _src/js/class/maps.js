module.exports = class Maps {
  constructor() {

    this.name = "Blue Bird"
    this.latlng = new google.maps.LatLng(35.7018361,139.7118785)
    this.options = {
      zoom: 13,
      center: this.latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    this.style = [
        {
            "featureType": "landscape", // 建物
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#dfdfdf"
                }
            ]
        },
        {
            "featureType": "poi", // 有名スポット
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                }, {
                  color: "#cad7e1"
                }
            ]
        },
        {
            "featureType": "transit", // 駅
            "elementType": "geometry.fill",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#cad7e1"
                }
            ]
        },
        {
            "featureType": "road", // 道
            "elementType": "geometry",
            "stylers": [
                {
                    "visibility": "simplified"
                },
                {
                    "color": "#ffffff"
                }
            ]
        },
        {
            "featureType": "road", // 道の名前
            "elementType": "labels",
            "stylers": [
                {
                    "visibility": "on"
                }, {
                  color: "#cad7e1"
                }
            ]
        },
        {
            "featureType": "transit.line", // 路線
            "elementType": "geometry",
            "stylers": [
                {
                    "visibility": "on"
                },
                {
                    "color": "#cad7e1"
                }
            ]
        },
        {
            "featureType": "water", // 水
            "elementType": "all",
            "stylers": [
                {
                    "color": "#7dcdcd"
                }
            ]
        }
    ]
    this.type = new google.maps.StyledMapType(this.style, { name: this.name })
    this.icon = new google.maps.MarkerImage('icon.png', new google.maps.Size(53,64), new google.maps.Point(0,0))
    this.markerOptions = {
      position: this.latlng,
      map: this.map,
      icon: this.icon,
      title: this.name
    }
  }

  init() {
    this.map = new google.maps.Map(document.getElementById('map_custmomize'), this.options)
    this.map.mapTypes.set('sample', this.type);
    this.map.setMapTypeId('sample');

    
    var marker = new google.maps.Marker(this.markerOptions);

  }
}











